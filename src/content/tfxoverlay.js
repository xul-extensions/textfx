if ("undefined" == typeof(TextFX)) {
	var TextFX = {};
}
const {classes: Cc, interfaces: Ci, utils: Cu} = Components,
ID_BOREALIS = "{a3210b97-8e8a-4737-9aa0-aa0e607640b9}";
Cu.import("chrome://textfx/content/tfxcommon.jsm",TextFX);
Cu.import("resource://gre/modules/Services.jsm");
TextFX.Launch = {
	mnuCtx : null,
	mnuTfx : null,
	cmdArrows : null,
	cmdDoublecross : null,
	cmdStrike : null,
	cmdUnderline : null,
	cmdCrossback : null,
	cmdCrosshatch : null,
	cmdSidebar : null,
	cmdSmallcaps : null,
	cmdMirrored : null,
	cmdUpsidedown : null,
	cmdCyrillic : null,
	cmdLeetspeak : null,
	cmdWaves : null,
	sidebar : null,
	docNode : null,
  cmdPrefs : null,
	selectedText : null,
  prefWindow : null,
	init : function(){
    TextFX.Common.logoutput = TextFX.Common.tfxPrefBranch.getBoolPref("logging",false);
		TextFX.Launch.cmdArrows = document.getElementById("CmdTfxArrowsunder");
    TextFX.Launch.cmdCrossback = document.getElementById("CmdTfxCrossback");
		TextFX.Launch.cmdCrosshatch = document.getElementById("CmdTfxCrosshatch");
		TextFX.Launch.cmdDoublecross = document.getElementById("CmdTfxDoublecross");
    TextFX.Launch.cmdSidebar = document.getElementById("CmdTfxShowTfxSidebar");
    TextFX.Launch.sidebar = document.getElementById("textfx-sidebar");
		TextFX.Launch.cmdStrike = document.getElementById("CmdTfxStrike");
    TextFX.Launch.cmdPrefs = document.getElementById("CmdTfxPrefs");
    TextFX.Launch.cmdSmallcaps = document.getElementById("CmdTfxSmallcaps");
    TextFX.Launch.cmdMirrored = document.getElementById("CmdTfxMirrored");
    TextFX.Launch.cmdUnderline = document.getElementById("CmdTfxUnderline");
    TextFX.Launch.cmdUpsidedown = document.getElementById("CmdTfxUpsidedown");
    TextFX.Launch.cmdWaves = document.getElementById("CmdTfxWavesabove");
    TextFX.Launch.cmdCyrillic = document.getElementById("CmdTfxCyrillic");
    TextFX.Launch.cmdLeetspeak = document.getElementById("CmdTfxLeetspeak");
		TextFX.Launch.mnuTfx = document.getElementById("menu-textfx");
		TextFX.Launch.mnuCtx = document.getElementById("contentAreaContextMenu");
    TextFX.Launch.cmdStrike.setAttribute("index","0");
    TextFX.Launch.cmdUnderline.setAttribute("index","1");
    TextFX.Launch.cmdCrosshatch.setAttribute("index","2");
    TextFX.Launch.cmdCrossback.setAttribute("index","4");
    TextFX.Launch.cmdDoublecross.setAttribute("index","3");
		TextFX.Launch.cmdArrows.setAttribute("index","5");
    TextFX.Launch.cmdWaves.setAttribute("index","6");
    TextFX.Launch.cmdSmallcaps.setAttribute("index","7");
    TextFX.Launch.cmdUpsidedown.setAttribute("index","8");
    TextFX.Launch.cmdMirrored.setAttribute("index","9");
    TextFX.Launch.cmdCyrillic.setAttribute("index","10");
    TextFX.Launch.cmdLeetspeak.setAttribute("index","11");
    TextFX.Launch.mnuCtx.addEventListener("popupshowing",TextFX.Launch.onContext,false);
		TextFX.Launch.cmdCrossback.addEventListener("command",TextFX.Launch.onCmd,false);
		TextFX.Launch.cmdCrosshatch.addEventListener("command",TextFX.Launch.onCmd,false);
		TextFX.Launch.cmdDoublecross.addEventListener("command",TextFX.Launch.onCmd,false);
		TextFX.Launch.cmdStrike.addEventListener("command",TextFX.Launch.onCmd,false);
		TextFX.Launch.cmdUnderline.addEventListener("command",TextFX.Launch.onCmd,false);
    TextFX.Launch.cmdArrows.addEventListener("command",TextFX.Launch.onCmd,false);
    TextFX.Launch.cmdWaves.addEventListener("command",TextFX.Launch.onCmd,false);
    TextFX.Launch.cmdSmallcaps.addEventListener("command",TextFX.Launch.onCmd,false);
    TextFX.Launch.cmdMirrored.addEventListener("command",TextFX.Launch.onCmd,false);
    TextFX.Launch.cmdUpsidedown.addEventListener("command",TextFX.Launch.onCmd,false);
    TextFX.Launch.cmdCyrillic.addEventListener("command",TextFX.Launch.onCmd,false);
    TextFX.Launch.cmdLeetspeak.addEventListener("command",TextFX.Launch.onCmd,false);
    TextFX.Launch.cmdSidebar.addEventListener("command",TextFX.Launch.onToolbarButton,false);
    TextFX.Launch.cmdPrefs.addEventListener("command",TextFX.Launch.showPrefs,false);
    TextFX.Launch.prefListener.register();
	},
	/**
	 * Context menu handler. 
	 */
	onContext : function(){
		gContextMenu.showItem(TextFX.Launch.mnuTfx,
    Services.prefs.getBoolPref("extensions.textfx.contextmenu",true) &&
    TextFX.Launch.getSelectedText().length > 0);
	},
  /**
   * Borealis specific function to display sidebar.
   */
  showSidebar : function(){
    var textFXPanel = document.getElementById("urn:sidebar:panel:viewTextFXSidebar");
      SidebarShowHide();
    if(textFXPanel && !sidebar_is_hidden()){
      SidebarSelectPanel(textFXPanel, true, true);
    }
  },
  /**
   * Event handler for toolbar button to toggle sidebar.
   */
  onToolbarButton : function(){
    if(Services.appinfo.ID==ID_BOREALIS){
      TextFX.Launch.showSidebar();
    } else {
      toggleSidebar("viewTextFXSidebar");
    }
  },
	/**
	 * Return currently selected text.
	 */
	getSelectedText: function() {
    	var node = document.popupNode;
        var selection = "";
        if ((node instanceof HTMLTextAreaElement) || 
        	(node instanceof HTMLInputElement && node.type == "text")|| node.isContentEditable) {
            	selection = node.value.substring(node.selectionStart, node.selectionEnd);
				TextFX.Launch.docNode = node;
        } else {
			TextFX.Launch.docNode = null;
		}
		return selection;
	},
	/**
	 * Common event handler for menu items.
	 */
	onCmd : function(event){
    	var node = document.popupNode;
        var selection = "";
        if ((node instanceof HTMLTextAreaElement) || (node instanceof HTMLInputElement && node.type == "text")) {
			var fullStr = node.value;
            selection = fullStr.substring(node.selectionStart, node.selectionEnd);
			if(selection.length > 0){
				var newVal = fullStr.substring(0,node.selectionStart)
        +TextFX.Common.applyFX(selection,Number(event.target.getAttribute("index")))
				+fullStr.substring(node.selectionEnd,fullStr.length);
				TextFX.Common.printlog("New value is "+newVal);
				node.value = newVal;
			}
		}
	},
  /**
   * Displays the preferences window.
   */
  showPrefs: function() {
    if (TextFX.Launch.prefWindow == null || TextFX.Launch.prefWindow.closed) {
      let instantApply =
        Services.prefs.getBoolPref("browser.preferences.instantApply", true);
      let features =
        "chrome,titlebar,centerscreen" +
        (instantApply.value ? ",dialog=no" : ",modal");
      TextFX.Launch.prefWindow = window.openDialog("chrome://textfx/content/tfxprefs.xul", "TextFX Preferences",
        features);
      TextFX.Launch.prefWindow.focus();
    }
  },
  prefListener : new TextFX.TextFXPrefListener(function(branch,name){
    switch(name){
      case "logging" : TextFX.Common.logoutput = branch.getBoolPref("logging",false); 
      break;
      case "showstrike" : TextFX.Launch.cmdStrike.hidden = !branch.getBoolPref("showstrike",true);
      break;
      case "showunderline" : TextFX.Launch.cmdUnderline.hidden = !branch.getBoolPref("showunderline",true);
      break;
      case "showxhatch" : TextFX.Launch.cmdCrosshatch.hidden = !branch.getBoolPref("showxhatch",true);
      break;
      case "showxxhatch" : TextFX.Launch.cmdDoublecross.hidden = !branch.getBoolPref("showxxhatch",true);
      break;
      case "showreversexhatch" : TextFX.Launch.cmdCrossback.hidden = !branch.getBoolPref("showreversexhatch",true);
      break;
      case "showarrows" : TextFX.Launch.cmdArrows.hidden = !branch.getBoolPref("showarrows",true);
      break;
      case "showwaves" : TextFX.Launch.cmdWaves.hidden = !branch.getBoolPref("showwaves",true);
      break;
      case "showmirrored" : TextFX.Launch.cmdMirrored.hidden = !branch.getBoolPref("showmirrored",true);
      break;
      case "showsmallcaps" : TextFX.Launch.cmdSmallcaps.hidden = !branch.getBoolPref("showsmallcaps",true);
      break;
      case "showupsidedown" : TextFX.Launch.cmdUpsidedown.hidden = !branch.getBoolPref("showupsidedown",true);
      break;
      case "showcyrillic" : TextFX.Launch.cmdCyrillic.hidden = !branch.getBoolPref("showcyrillic",true);
      break;
      case "showleetspeak" : TextFX.Launch.cmdLeetspeak.hidden = !branch.getBoolPref("showleetspeak",true);
      break;
      default:
      break;
    }
  }),
	cleanup : function(){
		TextFX.Launch.prefListener.unregister();
		TextFX.Launch.cmdPrefs.removeEventListener("command",TextFX.Launch.showPrefs);
     	TextFX.Launch.cmdSidebar.removeEventListener("command",TextFX.Launch.onToolbarButton);
     	TextFX.Launch.cmdUpsidedown.removeEventListener("command",TextFX.Launch.onCmd);
    	TextFX.Launch.cmdMirrored.removeEventListener("command",TextFX.Launch.onCmd);
    	TextFX.Launch.cmdSmallcaps.removeEventListener("command",TextFX.Launch.onCmd);
    	TextFX.Launch.cmdWaves.removeEventListener("command",TextFX.Launch.onCmd);
    	TextFX.Launch.cmdArrows.removeEventListener("command",TextFX.Launch.onCmd);
		TextFX.Launch.cmdUnderline.removeEventListener("command",TextFX.Launch.onCmd);
		TextFX.Launch.cmdStrike.removeEventListener("command",TextFX.Launch.onCmd);
		TextFX.Launch.cmdDoublecross.removeEventListener("command",TextFX.Launch.onCmd);
		TextFX.Launch.cmdCrosshatch.removeEventListener("command",TextFX.Launch.onCmd);
		TextFX.Launch.cmdCrossback.removeEventListener("command",TextFX.Launch.onCmd);
		TextFX.Launch.cmdCyrillic.removeEventListener("command",TextFX.Launch.onCmd);
		TextFX.Launch.cmdLeetspeak.removeEventListener("command",TextFX.Launch.onCmd);
		TextFX.Launch.mnuCtx.removeEventListener("popupshowing",TextFX.Launch.onContext);
	}
}
window.addEventListener("load",TextFX.Launch.init,false);
window.addEventListener("unload",TextFX.Launch.cleanup,false);