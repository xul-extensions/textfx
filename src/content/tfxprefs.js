if ("undefined" == typeof (TextFX)) {
  var TextFX = {};
}
Components.utils.import("chrome://textfx/content/tfxcommon.jsm", TextFX);
const { classes: Cc, interfaces: Ci, utils: Cu } = Components;
TextFX.Prefs={
  allDisabled : null,
  arrChkboxes : null,
  arrPerms : ["showstrike","showunderline","showxhatch","showxxhatch",
  "showreversexhatch","showarrows","showwaves","showupsidedown","showmirrored","showsmallcaps","showcyrillic","showleetspeak"],
  chkCtxMenu : null,
  init : function(){
    TextFX.Prefs.allDisabled = document.getElementById("brTfxAllDisabled");
    TextFX.Prefs.chkCtxMenu = document.getElementById("tfxmainpref-chkctxmenu");
    TextFX.Prefs.chkCtxMenu.addEventListener("command",TextFX.Prefs.onCtxMenu,false);
    TextFX.Prefs.arrChkboxes = document.querySelectorAll(`[id ^="tfxpref-chk"]`);
    for(let i=0;i<TextFX.Prefs.arrChkboxes.length;i++){
        TextFX.Prefs.arrChkboxes[i].addEventListener("command",TextFX.Prefs.onChkbox,false);
    }
    TextFX.Prefs.chkCtxMenu.checked = !TextFX.Prefs.allUnchecked();
  },
  /**
   * Tests if all effects checkboxes are unchecked.
   */
  allUnchecked : function(){
    let check = false;
    for(let i = 0;i<TextFX.Prefs.arrChkboxes.length;i++){
      check = check || TextFX.Prefs.arrChkboxes[i].checked;
      if(check){
        return false;
      }
    }
    return true;
  },
  /**
   * Event handler for effect checkboxes.
   */
  onChkbox : function(event){
    if(!event.target.checked){
        if(TextFX.Prefs.allUnchecked()){
          TextFX.Prefs.allDisabled.setAttribute("disabled","true");
          TextFX.Common.tfxPrefBranch.setBoolPref("contextmenu",false);
          return;
        }
    }
   TextFX.Prefs.allDisabled.removeAttribute("disabled");
  },
  /**
   * Event handler for context menu checkbox.
   */
  onCtxMenu : function(event){
    if(event.target.checked){
        TextFX.Prefs.allDisabled.removeAttribute("disabled");
        if(TextFX.Prefs.allUnchecked()){
          for(let i=0;i<TextFX.Prefs.arrPerms.length;i++){
             TextFX.Common.tfxPrefBranch.setBoolPref(TextFX.Prefs.arrPerms[i],true);
          }
        }
    } else {
        TextFX.Prefs.allDisabled.setAttribute("disabled","true");
    }
  },
  cleanup : function(){
    for(let i=0;i<TextFX.Prefs.arrChkboxes.length;i++){
        TextFX.Prefs.arrChkboxes[i].removeEventListener("command",TextFX.Prefs.onChkbox);
    }
    TextFX.Prefs.chkCtxMenu.removeEventListener("command",TextFX.Prefs.onCtxMenu);
  }  
}
window.addEventListener("load", TextFX.Prefs.init, false);
window.addEventListener("unload", TextFX.Prefs.cleanup, false);