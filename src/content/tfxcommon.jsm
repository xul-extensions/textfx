var EXPORTED_SYMBOLS = ["Common","TextFXPrefListener"]
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("resource://gre/modules/Services.jsm");
Common = {
    tfxPrefBranch : Services.prefs.getBranch("extensions.textfx."),
    logoutput : false,
    arrayStrike : ['\u0336','\u0332','\u0337','\u20EB','\u20E5','\u032D','\u0360'],
    // 0 = smallcaps, 1 = upside down, 2 = mirrored
    arraySubs :[{ a : '\u1D00', b : '\u0299', c : '\u1D04', d : '\u1D05', 
    e : '\u1D07', f : '\u0493', g : '\u0262', h : '\u029C', i : '\u026A', 
    j : '\u1D0A', k : '\u1D0B', l : '\u029F', m : '\u1D0D', n : '\u0274', 
    o : '\u1D0F', p : '\u1D18', q : '\u01EB', r : '\u0280', s : 's', 
    t : '\u1D1B', u : '\u1D1C', v : '\u1D20', w : '\u1D21', x : 'x', 
    y : '\u028F', z : '\u1D22' },
    { a : '\u0250', b : 'q', c : '\u0254', d : 'p', e : '\u01DD', f : '\u025F', 
    g : '\u0183', h : '\u0265', i : '\u0131', j : '\u027E', k : '\u029E', 
    l : '\u05DF', m : '\u026F', n : 'u', p : 'd', q : 'b', r : '\u0279',
    's' : '\u01A8', t : '\u0287', u : 'n', v : '\u028C', w : '\u028D', y : '\u028E', 
    '.' : '\u02D9', '[' : ']', '(' : ')', '{' : '}', '?' : '\u00BF', 
    '!' : '\u00A1', '\'' : ',', '<' : '>', '_' : '\u203E', '\"' : '\u201E', 
    ';' : '\u061B', '\u203F' : '\u2040', '\u2045' : '\u2046', '\u2234' : '\u2235', 
    'A' : '\u2200', 'B' : 'q', 'C' : '\u03FD', 'D' : '\u15E1',   'E' : '\u018E',
    'F' : '\u2132', 'G' : '\u0183',  'H' : 'H', 'I' : 'I', 'J' : '\u017F', 
    'K' : '\u029E', 'L' : '\u02E5', 'M' : 'W', 'N' : '\u041D',  'O' : 'O', 
    'P' : '\u0500', 'Q' : '\u1F49', 'R' : '\u1D1A', 'S' : '\uA644', 
    'T' : '\u22A5', 'U' : '\u2229', 'V' : '\u039B', 'W' : 'M', 'X' : 'X', 
    'Y' : '\u028E', 'Z' : 'Z' },
    {"?" : "\u2E2E", "0" : "0","1" : "\u0196","2" : "\u03C2","3" : "\u0190",
     "4" : "\u03BC", "5" : "\u091F", "6" : "\u10DB","7" : "\u0662","8" : "8",
     "9" : "\u0B67", "a" : "\u0252", "b" : "d","c" : "\u0254","d" : "b",
     "e" : "\u0258", "f" : "\u0287", "g" : "\u03F1","h" : "\u029C",
     "i" : "i","j" : "\u012F","k" : "\u029E","l" : "l","m" : "m","n" : "n",
     "o" : "o","p" : "q","q" : "p", "r" : "\u027F","s" : "\u01A8","t" : "\u0248",
     "u" : "\u03C5","v" : "v","w" : "w","x" : "x","y" : "\u03B3","z" : "z",
     "A" : "A","B" : "\u0A98","C" : "\u0186","D" : "\u10A7","E" : "\u018E",
     "F" : "\u11BF","G" : "\u04D8","H" : "\u0048","I" : "I", "J" : "\u10B1",
     "K" : "\uFEFC","L" : "\u2143","M" : "M","N" : "\u0418","O" : "O",
     "P" : "\u0533","Q" : "\u03D8","R" : "\u042F", "S" : "\u01A7",
     "T" : "T","U" : "U","V" : "V","W" : "W","X" : "X","Y" : "Y","Z" : "Z"},
    {'A': '\u0410', 'a': '\u0430', 'B': '\u0412', 'b': '\u0432', 'C': '\u0421', 'c': '\u0441',
  	 'D': 'D', 'd': 'd', 'E': '\u0415', 'e': '\u0435', 'F': 'F', 'f': 'f', 'G': 'G', 'g': 'g',
  	 'H': '\u041D', 'h': '\u043D', 'I': '\u0406', 'i': '\u0456', 'J': '\u0408', 'j': '\u0458',
  	 'K': '\u041A', 'k': '\u043A', 'L': 'L', 'l': 'l', 'M': '\u041C', 'm': '\u043C',
  	 'N': 'N', 'n': 'n', 'O': '\u041E', 'o': '\u043E', 'P': '\u0420', 'p': '\u0440',
  	 'Q': 'Q', 'q': 'q', 'R': 'R', 'r': 'r', 'S': 'S', 's': 's', 'T': 'T', 't': 't',
  	 'U': 'U', 'u': 'u', 'V': 'V', 'v': 'v', 'W': 'W', 'w': 'w', 'X': '\u0425', 'x': '\u0445',
  	 'Y': '\u0423', 'y': '\u0443', 'Z': 'Z', 'z': 'z'},
  	 {'A': '4', '4': 'a', 'B': '8', 'b': '8', 'C': 'C', 'c': 'c',
 	  'D': 'D', 'd': 'd', 'E': '3', 'e': '3', 'F': 'F', 'f': 'f',
 	  'G': '6', 'g': '6', 'H': 'H', 'h': 'h', 'I': '1', 'i': '1',
 	  'J': 'J', 'j': 'j', 'K': 'K', 'k': 'k', 'L': 'L', 'l': 'l',
 	  'M': 'M', 'm': 'm', 'N': 'N', 'n': 'n', 'O': '0', 'o': '0',
	   'P': 'P', 'p': 'p', 'Q': 'Q', 'q': 'q', 'R': 'R', 'r': 'r',
 	  'S': '5', 's': '5', 'T': '7', 't': '7', 'U': 'U', 'u': 'u',
 	  'V': 'V', 'v': 'v', 'W': 'W', 'w': 'w', 'X': 'X', 'x': 'x',
 	  'Y': 'Y', 'y': 'y', 'Z': '2', 'z': '2'}],
    /**
     * Log output to toolkit error console if enabled.
     */ 
    printlog : function(item) {
        if(Common.logoutput == true) {
            Services.console.logStringMessage("[TextFX : ]"+item);
        }
    },
    /**
     * Return passed in string updated with effect.
     */
    applyFX : function(str,index){
        var result = '';
        if(index >= 0 && index < Common.arrayStrike.length){
          result = Common.charInject(str,index);
        } else switch(index){
          case 7:
            result = Common.charSubstitute(str,Common.arraySubs[index-Common.arrayStrike.length],false);
          break;
          case 8:
            result = Common.charSubstitute(str,Common.arraySubs[index-Common.arrayStrike.length],true);
          break;
          case 9:
            result = Common.charSubstitute(str,Common.arraySubs[index-Common.arrayStrike.length],true);
          break;
          case 10:
            result = Common.charSubstitute(str,Common.arraySubs[index-Common.arrayStrike.length],false);
          break;
          case 11:
            result = Common.charSubstitute(str,Common.arraySubs[index-Common.arrayStrike.length],false);
          break;
        }
        return result;
    },
    /**
     * Injects the character for strikeout type effects.
     * @param {} str String to substitute.
     * @param {} index Array index of effect to use.
     * @return {} Modified string.
     */
    charInject : function(str,index){
        var result = '';
        for(var i = 0; i < str.length;i++){
            var c = str.charAt(i);
            var r = c+Common.arrayStrike[index];
            result += r;
        }
        return result;
    },
    /**
     * Substitutes the character for upside down, smallcaps effects.
     * @param {} str String to substitute.
     * @param {} table Array of effect to use.
     * @return {} Modified string.
     */
    charSubstitute : function(str,table, flip){
      var last = str.length - 1;
      var result = new Array(str.length);
      for (var i = flip ? last : 0; flip ? i >= 0 : i <= last; flip ? --i : i++) {
        var c = str.charAt(i);
        var r = table[c];
        result[flip ? last - i : i] = r != undefined ? r : c;
      }
      var ret = result.join('');
      return ret;
    }
}
function TextFXPrefListener(callback) {
  // Keeping a reference to the observed preference branch or it will get
  // garbage collected.
  this._branch = Services.prefs.getBranch("extensions.textfx.");
  this._branch.QueryInterface(Ci.nsIPrefBranch2);
  this._callback = callback;
}

TextFXPrefListener.prototype.observe = function(subject, topic, data) {
  if (topic == 'nsPref:changed')
    this._callback(this._branch, data);
};

TextFXPrefListener.prototype.register = function() {
  this._branch.addObserver('', this, false);
  let that = this;
  this._branch.getChildList('', {}).
  forEach(function (pref_leaf_name){ 
    that._callback(that._branch, pref_leaf_name); 
  });
};

TextFXPrefListener.prototype.unregister = function() {
  if (this._branch)
    this._branch.removeObserver('', this);
};