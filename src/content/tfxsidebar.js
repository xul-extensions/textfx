if ("undefined" == typeof(TextFX)) {
  var TextFX = {};
}
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("chrome://textfx/content/tfxcommon.jsm",TextFX);
Cu.import("resource://gre/modules/Services.jsm");
TextFX.Sidebar={
  btnClear : null,
  cmdClear : null,
  txtInput : null,
  txtOutput : null,
  cmbFX : null,
  init : function(){
    TextFX.Sidebar.btnClear = document.getElementById("textfx-btnclear");
    TextFX.Sidebar.cmdClear = document.getElementById("CmdTfxClear");
    TextFX.Sidebar.txtInput = document.getElementById("textfx-input");
    TextFX.Sidebar.txtOutput = document.getElementById("textfx-output");
    TextFX.Sidebar.cmbFX = document.getElementById("textfx-cmbFx");
    TextFX.Sidebar.chkCtxMenu = document.getElementById("textfx-chkctxmenu");
    TextFX.Sidebar.btnClear.setAttribute('disabled','true');
    TextFX.Sidebar.txtInput.addEventListener("input",TextFX.Sidebar.doStrike,false);
    TextFX.Sidebar.txtOutput.addEventListener("click",TextFX.Sidebar.onClick,false);
    TextFX.Sidebar.cmbFX.addEventListener("command",TextFX.Sidebar.doStrike,false);
    TextFX.Sidebar.cmdClear.addEventListener("command",TextFX.Sidebar.onClear,false);
  },
  /**
   * Output textfield event handler to select all text when clicked.
   */
  onClick : function(event){
    event.target.focus();
    event.target.select();
  },
  /**
   * Clear button event handler.
   */
  onClear : function(){
    TextFX.Sidebar.txtInput.value="";
    TextFX.Sidebar.txtOutput.value="";
    TextFX.Sidebar.btnClear.setAttribute('disabled','true');
  },
  /**
   * Input textfield event handler.
   */
  doStrike : function(){
    if(TextFX.Sidebar.txtInput.value.length > 0){
      TextFX.Sidebar.txtOutput.value = TextFX.Common.applyFX(TextFX.Sidebar.txtInput.value,TextFX.Sidebar.cmbFX.selectedIndex);
      TextFX.Sidebar.txtOutput.select();
      TextFX.Sidebar.txtInput.focus();
      TextFX.Sidebar.btnClear.removeAttribute('disabled');
    } else {
      TextFX.Sidebar.btnClear.setAttribute('disabled','true');
      TextFX.Sidebar.txtOutput.value='';
    }
  },
  cleanup : function(){
    TextFX.Sidebar.cmdClear.removeEventListener("command",TextFX.Sidebar.onClear);
    TextFX.Sidebar.txtOutput.removeEventListener("click",TextFX.Sidebar.onClick);
    TextFX.Sidebar.txtInput.removeEventListener("input",TextFX.Sidebar.doStrike);
    TextFX.Sidebar.txtInput.value = '';
  }
}
window.addEventListener("load",TextFX.Sidebar.init,false);
window.addEventListener("unload",TextFX.Sidebar.cleanup,false);